/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDExamples

import BeSpaceDCore._



object BeSpaceDVerify {

  def main(args: Array[String]) {

    val core = standardDefinitions
    import core._

    val something = BIGAND(List(IMPLIES(TimePoint(2345), ComponentState("any")), OR(OccupyNode(44), OccupyPoint(1, 2))))

    val simplifiedSomething = simplifyInvariant(something)

    println("BeSpaceD VERIFICATION")
    println("=====================")

    println(simplifiedSomething)
    
    require(simplifiedSomething.toString == "AND(IMPLIES(TimePoint(2345),ComponentState(any)),OR(OccupyNode(44),OccupyPoint(1,2)))")
  }
}

/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/* 
 * Created 11/09/2013
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples

import java.util.Date
//import scala.compat.Platform.EOL

import BeSpaceDCore._
import BeSpaceDExamples.BlechHerrmannExample1._



object BenchmarkExample1 extends CoreDefinitions{

  def main(args: Array[String]) {

    val COUNT = 10    // The number of times to repeat the benchmark tests before taking the average.

		val times = List.fill(COUNT)
		{

			// ---------------------------------------------------------------------- Topological / Geometric
			val topolicicalStart = new Date().getTime

			testTopologicalGeometric()

			val topologicalTime = new Date().getTime - topolicicalStart


			// ---------------------------------------------------------------------- Physics

			val physicsStart = new Date().getTime
			testPhysics()

			val physicsTime = new Date().getTime - physicsStart



			// ---------------------------------------------------------------------- Comparison Arctis

			val arctisStart = new Date().getTime

			testComparisonArctis()

			val arctisTime = new Date().getTime - arctisStart



			// ---------------------------------------------------------------------- New Comparison Arctis

			val newArctisStart = new Date().getTime

			testNewComparisonArctis()

			val newArctisTime = new Date().getTime - newArctisStart



			// ---------------------------------------------------------------------- Next Version of New Comparison Arctis

			val nextNewArctisStart = new Date().getTime

			testNextNewComparisonArctis()

			val nextNewArctisTime = new Date().getTime - nextNewArctisStart



			// ================ Aggregate all the tests into one tuple

			(topologicalTime, physicsTime, arctisTime, newArctisTime, nextNewArctisTime)
		}



		def sum(t1:(Long,Long,Long,Long,Long), t2:(Long,Long,Long,Long,Long)): (Long,Long,Long,Long,Long) =
		{
			(t1._1 + t2._1, t1._2 + t2._2, t1._3 + t2._3, t1._4 + t2._4, t1._5 + t2._5)
		}

		val totalTimes: List[Long] = { t: (Long,Long,Long,Long,Long) => t._1 :: t._2 :: t._3 :: t._4 :: t._5 :: Nil } apply (times reduce sum _)

		val averageTimes: List[Long] = totalTimes map { _ / COUNT }

		val (topologicalTime: Long, physicsTime: Long, arctisTime: Long, newArctisTime: Long, nextNewArctisTime: Long) = (averageTimes(0), averageTimes(1), averageTimes(2), averageTimes(3), averageTimes(4))


		// ---------------------------------------------------------------------- Benchmarking Report

		/*
     * Here are three good runs (COUNT = 10) on a MacBook Pro with:
     *      3.1 GHz Intel Core i7
     *      16 GB 1867 MHz DDR3
     *
     *      BENCHMARK SUMMARY
     *      =================
     *                     Topological Invariants: 1789
     *                          Comparison Arctis: 52
     *                      New Comparison Arctis: 84
     *      Next Version of New Comparison Arctis: 78
     *
     *      BENCHMARK SUMMARY
     *      =================
     *                     Topological Invariants: 1797 (100%)
     *                          Comparison Arctis: 50 (104%)
     *                      New Comparison Arctis: 72 (117%)
     *      Next Version of New Comparison Arctis: 83 (94%)
     *
     *      BENCHMARK SUMMARY
     *      =================
     *                     Topological Invariants: 1632 (110%)
     *                          Comparison Arctis: 48 (104%)
     *                      New Comparison Arctis: 71 (101%)
     *      Next Version of New Comparison Arctis: 70 (111%)
     *
     * The best of these results will become the reference benchmark for comparisons.
     *
     */

		val TOPOLOGICAL_REFERENCE     = 1632
		val ARCTIS_REFERENCE          = 48
		val NEW_ARCTIS_REFERENCE      = 71
		val NEXT_NEW_ARCTIS_REFERENCE = 70

		def benchmark(actual: Long, reference: Long): String =
		{
			val percent: Long = Math.round(reference.toDouble/actual*100)

			s"$actual (${percent}%)"
		}

		println ()
		println ()
		//println (s"     BENCHMARK SUMMARY$EOL     =================")

		println (s"                    Topological Invariants: ${benchmark(topologicalTime, TOPOLOGICAL_REFERENCE)}")
		println (s"                         Comparison Arctis: ${benchmark(arctisTime, ARCTIS_REFERENCE)}")
		println (s"                     New Comparison Arctis: ${benchmark(newArctisTime, NEW_ARCTIS_REFERENCE)}")
		println (s"     Next Version of New Comparison Arctis: ${benchmark(nextNewArctisTime, NEXT_NEW_ARCTIS_REFERENCE)}")

	}

}
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap and Vasileios Dimitrakopoulos 2016
 */


package model;

import java.util.Date;

import BeSpaceDCore.Invariant;
import BeSpaceDCore.TimePoint;
import edu.ufl.digitalworlds.j4k.DepthMap;
import edu.ufl.digitalworlds.j4k.J4KSDK;
import view.App;

/**
 * Singleton
 */
public final class KinectSensor extends J4KSDK {

    private static KinectSensor INSTANCE;
    public static final int SMOOTH_FACTOR = 5;
    private FrameProcessor processor;
    private boolean snapshotMode = true;
    private boolean depthDone = false;
    private boolean colorDone = false;
    
    /**
     * Kinect INSTANCE private constructor.
     * @param kinect_type The type of the INSTANCE.
     * @param colorContent The Color window to drawArea on.
     * @param depthContent The Depth window to drawArea on.
     */
    private KinectSensor(byte kinect_type) {
        super(kinect_type);
        processor = FrameProcessor.getInstance();
    }
    
    
    /**
     * Static model.
     * @return the single instance of the model.
     */
    public static KinectSensor getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new KinectSensor(J4KSDK.MICROSOFT_KINECT_2);
        }
        return INSTANCE;
    }
    
    
    /**
     * Prints the Kinect INSTANCE's details.
     */
    public void printDetails() {
        System.out.println("----- Kinect Device Information -----");
        System.out.println("Device Type: " + getDeviceType());
        System.out.println("Color Resolution: " + getColorWidth() + "x" + getColorHeight());
        System.out.println("Depth Resolution: " + getDepthWidth() + "x" + getDepthHeight());
    }
    
    
    @Override
    public void onColorFrameEvent(byte[] color_data) {
        if (!depthDone) {
            processor.setLatestColorData(color_data);
            if (snapshotMode) {
                depthDone = true;
                saveDataset();
            }
        }
    }
    
    @Override
    public void onDepthFrameEvent(short[] depth_frame, byte[] player_index, float[] XYZ, float[] UV) {
        if (!colorDone) {
            DepthMap map = new DepthMap(App.DEPTH_WIDTH, App.DEPTH_HEIGHT, XYZ);
            map.setUV(UV);
            processor.setLatestDepthMap(map);
            if (snapshotMode) {
                colorDone = true;
                saveDataset();            }
        }
    }
    
    @Override
    public void onSkeletonFrameEvent(boolean[] arg0, float[] arg1, float[] arg2,
            byte[] arg3) {
        // TODO
    	System.out.println();
    	System.out.println("SkeletonFrameEvent");
    	printDetails();
     }

    public void setSnapshotMode(boolean snapshotMode) {this.snapshotMode = snapshotMode;}
    
    private
    void saveDataset()
    {
    	KinectScan kinectScan = new KinectScan(processor.getXYZ(), processor.getUV(), new float[]{}, processor.getColorData());
    	
		System.out.println("kinectScan" + kinectScan);
		System.out.println("   #kinectScan.xyz        = " + kinectScan.xyz().length);
		System.out.println("   #kinectScan.uv         = " + kinectScan.uv().length);
		System.out.println("   #kinectScan.color_data = " + kinectScan.color_data().length);
		
		// Convert data to BeSpaceD Invariant
		TimePoint<Date> now = new TimePoint<Date>(new Date());
		Invariant invariant = ImportExportDouble$.MODULE$.importKinectScanTime(kinectScan, now);
		
		// Save the data set
		BeSpaceDData.package$.MODULE$.save(invariant, "aicause.kinect.scan.obstacles16");
		BeSpaceDData.package$.MODULE$.saveReadable(invariant, "aicause.kinect.scan.obstacles16");
    }

}
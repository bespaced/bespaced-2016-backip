package SMTOperations

import com.microsoft.z3._;
//import java.util.HashMap;
//import tooloperations._;
//import examples.BlechHerrmannExample1;
import BeSpaceDCore._;


object BlechHerrmannTest1 extends CoreOperations {

  def timetests1 : Boolean ={
    val cdefs = new CoreDefinitions();
    var retval = false;
    for (i<- 0 to 240) {
      (cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,BlechHerrmannExample1.robotinvariantAbs1wt()))), cdefs.simplifyInvariant((cdefs.getSubInvariantForTime(i,BlechHerrmannExample1.humanapproachingLeft())))) match  {
        case (OccupyBox (c1x1,c1y1,c1x2,c1y2) ,OccupyBox (c2x1,c2y1,c2x2,c2y2)) => retval |= check2BoxSMT(c1x1,c1y1,c1x2,c1y2,c2x1,c2y1,c2x2,c2y2);
        case (i1,i2) => println("Error");
      }
     } 
    return retval;
  }
  
  def main(args : Array[String]) {
   var time = System.currentTimeMillis();
   timetests1; 
   println("Time needed :"+ (System.currentTimeMillis() - time) + "milliseconds");

    
    
    
  }
}
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDOPC

/**
 * @author keith
 */

import scala.collection.JavaConverters._
//import scala.collection.JavaConversions._

// OPC Foundation
import org.opcfoundation.ua.core.Identifiers
import org.opcfoundation.ua.core.ReferenceDescription
import org.opcfoundation.ua.core.Attributes

import org.opcfoundation.ua.common.NamespaceTable

import org.opcfoundation.ua.builtintypes.NodeId
import org.opcfoundation.ua.builtintypes.DataValue


// Prosys OPC UA
import com.prosysopc.ua.client.UaClient
import com.prosysopc.ua.client.AddressSpace



class BeOpcUaNode(val client: UaClient, val nodeId: NodeId) {
  
  private val addressSpace: AddressSpace              = client.getAddressSpace
  private val namespaceTable                          = addressSpace.getNamespaceTable
  private val references: List[ReferenceDescription]  = addressSpace.browse(nodeId).asScala.toList

  
  
  // PRINT
  def print =
  {
    println(s"NODE: ${this.nodeId.getValue} : ${this.nodeId.toString}")
    references.foreach { ref => println(s"${references.indexOf(ref)} - ${referenceToString(client , ref)}") }
  }
  
  
  
  // BROWSE
  def browse(browseName: String): BeOpcUaNode =
  {
    val ref = references.find { ref => ref.getBrowseName.getName == browseName}
    
    println(s"ref = $ref")
    
    val destinationNode: BeOpcUaNode = ref match
    {
      case Some(r) =>
        { 
        val expandedNodeId = r.getNodeId()
        new BeOpcUaNode(client, namespaceTable.toNodeId(expandedNodeId))
        }
        
      case _ => {
        println(s"Reference NOT FOUND: $browseName not in ${references.map(_.getBrowseName)}")
        this
        }
    }
    
    destinationNode
  }
  
  
  // READ VALUE
  def readValue: DataValue =  client.readAttribute(this.nodeId, Attributes.Value)

  
  // WRITE VALUE
  def writeValue[T](value: T): Boolean =  client.writeAttribute(this.nodeId, Attributes.Value, value)
 
}

object BeOpcUaNode {
  
  //def apply(client: UaClient, nodeId: NodeId): BeOpcUaNode =  new BeOpcUaNode(client, nodeId)
  
  def root(client: UaClient): BeOpcUaNode =  new BeOpcUaNode(client, Identifiers.RootFolder)
  
}
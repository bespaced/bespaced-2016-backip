/*
 * *******************************************************
 * This is a program for operating the FESTO stations
 * with the RaspberryPis using the Pi4J libraries
 * 
 * This program operates stations 1, 2, and 3.
 * *******************************************************
 * RMIT University
 * Last updated 5/31/16
 * Program by Yvette Wouters
 * *******************************************************
*/


// Libraries
import java.util.concurrent.Callable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

// Default for GPIO
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
// For triggers
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.pi4j.io.gpio.trigger.GpioPulseStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSetStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSyncStateTrigger;
// For state listeners
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.event.PinEventType;

// BeSPaceD Festo Server
// Currently all in the default package


@SuppressWarnings("unused")
public class FestoRaspberryPiDemonstrator {
	
	public static void main(String args[]) throws InterruptedException {
		
		//Station number to run 
		int stationNum = 0;
		
		//Check if integer and then parse if so
		if (args.length > 0)
		{
		    try
		    {
		    	stationNum = Integer.parseInt(args[0]);
		    } catch (NumberFormatException e)
		    {
		        System.err.println("Argument" + args[0] + " must be an int.");
		        System.exit(1);
		    }
		}
		
		//Check if station is valid
		if (stationNum == 1 || stationNum == 2 || stationNum == 3 || stationNum == 4 || stationNum == 5)
		{
			// Create gpio controller instance
			final GpioController gpio = GpioFactory.getInstance();
			
			//Make inputs hashtable	
			Hashtable<String, GpioPinDigitalInput> hashtableInputs = new Hashtable<String, GpioPinDigitalInput>();
			//Make outputs hashtable	
			final Hashtable<String, GpioPinDigitalOutput> hashtableOutputs = new Hashtable<String, GpioPinDigitalOutput>();
			
			//Add shutdown hook
			Runtime.getRuntime() .addShutdownHook(new Thread() {
				@Override
				public void run()
				{
					Print(" Quit!");
					
					// Set all outputs to 'off'
					SetAllHigh(hashtableOutputs);
					
					// Shut down GPIO
					gpio.shutdown();
				}
			});
			
			// Assign GPIO inputs and outputs
			if (stationNum == 1){AssignStation1GPIO(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 2){AssignStation2GPIO(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 3){AssignStation3GPIO(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 4){AssignStation4GPIO(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 5){AssignStation5GPIO(gpio, hashtableInputs, hashtableOutputs);}
			
			PrintAllInputStates(hashtableInputs);
			PrintAllOutputStates(hashtableOutputs);
			
			// Set the shutdown options for the output pins
			SetShutdownOptions(hashtableOutputs);
			// Add listeners for all pins
			AddListeners(hashtableInputs, hashtableOutputs);	
			
			// Run station
			if (stationNum == 1){RunStation1(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 2){RunStation2(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 3){RunStation3(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 4){RunStation4(gpio, hashtableInputs, hashtableOutputs);}
			else if (stationNum == 5){RunStation5(gpio, hashtableInputs, hashtableOutputs);}
		}
		else
		{
			Print("Station number does not exist. Exiting program.");
			System.exit(1);
		}
	}
	
	// Initialise and assign station 1 pins to hashtables
	public static void AssignStation1GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput stackEjectorExtendedLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "stackEjectorExtendedLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput stackEjectorRetractedLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "stackEjectorRetractedLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput workpieceGrippedSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "workpieceGrippedSensor", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput loaderPickupLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "loaderPickupLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput loaderDropoffLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "loaderDropoffLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput stackEmptySensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "stackEmptySensor", PinPullResistance.PULL_UP);
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", stackEjectorExtendedLS);
		hashtableInputs.put("GPIO_03", stackEjectorRetractedLS);
		hashtableInputs.put("GPIO_21", workpieceGrippedSensor);
		hashtableInputs.put("GPIO_22", loaderPickupLS);
		hashtableInputs.put("GPIO_23", loaderDropoffLS);
		hashtableInputs.put("GPIO_25", stackEmptySensor);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput stackEjectorExtendSol = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "stackEjectorExtendSol", PinState.HIGH);
		final GpioPinDigitalOutput vacuumGripperSol = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "vacuumGripperSol", PinState.HIGH);
		final GpioPinDigitalOutput ejectionAirPulseSol = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "ejectionAirPulseSol", PinState.HIGH);
		final GpioPinDigitalOutput loaderPickupSol = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "loaderPickupSol", PinState.HIGH);
		final GpioPinDigitalOutput loaderDropoffSol = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "loaderDropoffSol", PinState.HIGH);
				
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", stackEjectorExtendSol);
		hashtableOutputs.put("GPIO_05", vacuumGripperSol);
		hashtableOutputs.put("GPIO_06", ejectionAirPulseSol);
		hashtableOutputs.put("GPIO_26", loaderPickupSol);
		hashtableOutputs.put("GPIO_27", loaderDropoffSol);
	}
	
	// Initialise and assign station 2 pins to hashtables
	public static void AssignStation2GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput firstConveyorLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "firstConveyorLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput secondConveyorLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "secondConveyorLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput lastConveyorLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "lastConveyorLS", PinPullResistance.PULL_UP);

		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", firstConveyorLS);
		hashtableInputs.put("GPIO_03", secondConveyorLS);
		hashtableInputs.put("GPIO_21", lastConveyorLS);
		
		// Outputs
		final GpioPinDigitalOutput conveyorBeltGate = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "conveyorBeltGate", PinState.HIGH);
		final GpioPinDigitalOutput conveyorBeltMove = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "conveyorBeltMove", PinState.HIGH);
		
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", conveyorBeltGate);
		hashtableOutputs.put("GPIO_05", conveyorBeltMove);
	}
	
	// Initialise and assign station 3 pins to hashtables
	public static void AssignStation3GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput endOfConveyorLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "endOfConveyorLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput conveyorPositionSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "conveyorPositionSensor", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput dropoffPositionSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "dropoffPositionSensor", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput middlePositionSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "middlePositionSensor", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput armLoweredSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "armLoweredSensor", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput armRaisedSensor = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "armRaisedSensor", PinPullResistance.PULL_UP);
		//final GpioPinDigitalInput workpieceGrippedLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_, "workpieceGrippedLS", PinPullResistance.PULL_UP);

		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", endOfConveyorLS);
		hashtableInputs.put("GPIO_03", conveyorPositionSensor);
		hashtableInputs.put("GPIO_21", dropoffPositionSensor);
		hashtableInputs.put("GPIO_22", middlePositionSensor);
		hashtableInputs.put("GPIO_23", armLoweredSensor);
		hashtableInputs.put("GPIO_25", armRaisedSensor);
		//hashtableInputs.put("GPIO_", workpieceGrippedLS);
		
		// Outputs
		final GpioPinDigitalOutput moveToConveyorBelt = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "moveToConveyorBelt", PinState.HIGH);
		final GpioPinDigitalOutput moveFromConveyorBelt  = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "moveFromConveyorBelt", PinState.HIGH);
		final GpioPinDigitalOutput lowerArm = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "lowerArm", PinState.HIGH);
		final GpioPinDigitalOutput gripWorkpiece = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "gripWorkpiece", PinState.HIGH);
		
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", moveToConveyorBelt);
		hashtableOutputs.put("GPIO_05", moveFromConveyorBelt);
		hashtableOutputs.put("GPIO_06", lowerArm);
		hashtableOutputs.put("GPIO_26", gripWorkpiece);
	}
	
	// Initialise and assign station 4 pins to hashtables
	public static void AssignStation4GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput input1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "input1", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input2 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "input2", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput enteringRotaryTableLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "enteringRotaryTableLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input4 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "input4", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput liquidFillerLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "liquidFillerLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput granularFillerLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "granularFillerLS", PinPullResistance.PULL_UP);
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", input1);
		hashtableInputs.put("GPIO_03", input2);
		hashtableInputs.put("GPIO_21", enteringRotaryTableLS);
		hashtableInputs.put("GPIO_22", input4);
		hashtableInputs.put("GPIO_23", liquidFillerLS);
		hashtableInputs.put("GPIO_25", granularFillerLS);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output3", PinState.HIGH);
	
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", output1);
		hashtableOutputs.put("GPIO_05", output2);
		hashtableOutputs.put("GPIO_06", output3);
	}
	
	// Initialise and assign station 5 pins to hashtables
	public static void AssignStation5GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput dropLidLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "dropLidLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput screwLidLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "screwLidLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput bottleFinishLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "bottleFinishLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput enterConveyorLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "enterConveyorLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput threeBottlesLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "threeBottlesLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input6 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input6", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input7 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input7", PinPullResistance.PULL_UP);
		
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", dropLidLS);
		hashtableInputs.put("GPIO_03", screwLidLS);
		hashtableInputs.put("GPIO_21", bottleFinishLS);
		hashtableInputs.put("GPIO_22", enterConveyorLS);
		hashtableInputs.put("GPIO_23", threeBottlesLS);
		hashtableInputs.put("GPIO_25", input6);
		hashtableInputs.put("GPIO_25", input7);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output3", PinState.HIGH);
		final GpioPinDigitalOutput output4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "output4", PinState.HIGH);
		final GpioPinDigitalOutput output5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output5", PinState.HIGH);
				
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", output1);
		hashtableOutputs.put("GPIO_05", output2);
		hashtableOutputs.put("GPIO_06", output3);
		hashtableOutputs.put("GPIO_26", output4);
		hashtableOutputs.put("GPIO_27", output5);
	}
	
	// Initialise and assign station 6 pins to hashtables
	public static void AssignStation6GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput input1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "input1", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input2 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "input2", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input3 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "input3", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input4 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "input4", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input5 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "input5", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input6 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input6", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input7 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input7", PinPullResistance.PULL_UP);
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", input1);
		hashtableInputs.put("GPIO_03", input2);
		hashtableInputs.put("GPIO_21", input3);
		hashtableInputs.put("GPIO_22", input4);
		hashtableInputs.put("GPIO_23", input5);
		hashtableInputs.put("GPIO_25", input6);
		hashtableInputs.put("GPIO_25", input7);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput moveConveyor = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "moveConveyor", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output3", PinState.HIGH);
		final GpioPinDigitalOutput output4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "output4", PinState.HIGH);
		final GpioPinDigitalOutput output5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output5", PinState.HIGH);
		final GpioPinDigitalOutput output6 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output6", PinState.HIGH);
		final GpioPinDigitalOutput output7 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output7", PinState.HIGH);
		final GpioPinDigitalOutput output8 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output8", PinState.HIGH);
		
		
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", moveConveyor);
		hashtableOutputs.put("GPIO_05", output2);
		hashtableOutputs.put("GPIO_06", output3);
		hashtableOutputs.put("GPIO_26", output4);
		hashtableOutputs.put("GPIO_27", output5);
		hashtableOutputs.put("GPIO_27", output6);
		hashtableOutputs.put("GPIO_27", output7);
		hashtableOutputs.put("GPIO_27", output8);
	}
	
	// Initialise and assign station 7 pins to hashtables
	public static void AssignStation7GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput oneBottleLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "oneBottleLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput threeBottlesLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "threeBottlesLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput sixBottlesLS = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "sixBottlesLS", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input4 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "input4", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input5 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "input5", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input6 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input6", PinPullResistance.PULL_UP);
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", oneBottleLS);
		hashtableInputs.put("GPIO_03", threeBottlesLS);
		hashtableInputs.put("GPIO_21", sixBottlesLS);
		hashtableInputs.put("GPIO_22", input4);
		hashtableInputs.put("GPIO_23", input5);
		hashtableInputs.put("GPIO_25", input6);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
	
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", output1);
		hashtableOutputs.put("GPIO_05", output2);
	}
	
	// Initialise and assign station 8 pins to hashtables
	public static void AssignStation8GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput input1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "input1", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input2 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "input2", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input3 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "input3", PinPullResistance.PULL_UP);

		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", input1);
		hashtableInputs.put("GPIO_03", input2);
		hashtableInputs.put("GPIO_21", input3);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output3", PinState.HIGH);
		final GpioPinDigitalOutput output4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "output4", PinState.HIGH);
		
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", output1);
		hashtableOutputs.put("GPIO_05", output2);
		hashtableOutputs.put("GPIO_06", output3);
		hashtableOutputs.put("GPIO_26", output4);
	}
	
	// Initialise and assign station 9 pins to hashtables
	public static void AssignStation9GPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput input1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "input1", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input2 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, "input2", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input3 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_21, "input3", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input4 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_22, "input4", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input5 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23, "input5", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input6 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input6", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input7 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input7", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input8 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_25, "input8", PinPullResistance.PULL_UP);
		
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_00", input1);
		hashtableInputs.put("GPIO_03", input2);
		hashtableInputs.put("GPIO_21", input3);
		hashtableInputs.put("GPIO_22", input4);
		hashtableInputs.put("GPIO_23", input5);
		hashtableInputs.put("GPIO_25", input6);
		hashtableInputs.put("GPIO_25", input7);
		hashtableInputs.put("GPIO_25", input8);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output3", PinState.HIGH);
		final GpioPinDigitalOutput output4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, "output4", PinState.HIGH);
		final GpioPinDigitalOutput output5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output5", PinState.HIGH);
		final GpioPinDigitalOutput output6 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, "output6", PinState.HIGH);
		
		
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_04", output1);
		hashtableOutputs.put("GPIO_05", output2);
		hashtableOutputs.put("GPIO_06", output3);
		hashtableOutputs.put("GPIO_26", output4);
		hashtableOutputs.put("GPIO_27", output5);
		hashtableOutputs.put("GPIO_27", output6);
	}
	
	//Runs 1st station
	public static void RunStation1(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException{
			
		Print("FESTO Station 1");
		Print("Press 'CTRL + C' to quit");

		// Initialise state
		int state = 0;
		// To keep track of any pieces currently on station 2
		boolean conveyorClear = true;

		try{
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				// Reset state
				if (state == 0)
				{
					Print("State 0 - Assess current state");
					
					// If a workpiece is currently being gripped
					if (CheckHigh(hashtableInputs, "GPIO_21") == true)	
					{
						// Proceed to state 6
						state = 6;
					}
					else
					{
						// If loader is in pickup position
						if (CheckHigh(hashtableInputs, "GPIO_22") == true)
						{
							// Proceed to state 5
							state = 5;
						}
						else
						{
							// Proceed to state 2
							state = 2;
						}
					}
				}
				// Wait
				else if (state == 1)
				{
					Print("State 1 - Wait for clearance");
					
					// Wait until the conveyor belt is cleared
					while (conveyorClear == false)
					{
						//Wait
					}
					
					state = 2;

				}				
				// Check for caps in tube
				else if (state == 2)
				{
					Print("State 2 - Check for caps in feeder");
					
					// If there are no caps in the tube
					if (CheckHigh(hashtableInputs, "GPIO_25") == true)
					{
						Print("No caps in feeder, please place at least one in feeder");
	
						// Sleep for 3 seconds
						Thread.sleep(3000);
					}
					else
					{
						Print("Caps are present, proceeding");
	
						// Extend stack ejector
						SetLow(hashtableOutputs, "GPIO_04");
	
						// Proceed to state 3
						state = 3;
					}
				}
				// Eject workpiece
				else if (state == 3)
				{
					Print("State 3 - Eject workpiece");
	
					// Wait until stack ejector is retracted
					WaitForInput(hashtableInputs, "GPIO_03", true);
	
					// Stop extending stack ejector (retracts and pushes out cap)	
					SetHigh(hashtableOutputs, "GPIO_04");
	
					// Wait until stack ejector is extended
					WaitForInput(hashtableInputs, "GPIO_00", true);
	
					// Proceed to state 4
					state = 4;
				}
				// Move loader to pick up position
				else if (state == 4)
				{
					Print("State 4 - Move loader to pick up position");
	
					// Turn on move to loader pickup position
					SetLow(hashtableOutputs, "GPIO_26");
	
					// Wait for loader to move to pickup position
					WaitForInput(hashtableInputs, "GPIO_22", true);
	
					// Turn off move to loader pickup position
					SetHigh(hashtableOutputs, "GPIO_26");
	
					// Proceed to state 5
					state = 5;
				}
				// Grip workpiece
				else if (state == 5)
				{
					Print("State 5 - Grip workpiece");
	
					// Turn on vacuum gripper
					SetLow(hashtableOutputs, "GPIO_05");
	
					// Wait for piece to be gripped
					WaitForInput(hashtableInputs, "GPIO_21", true);
	
					// Proceed to state 6
					state = 6;
				}
				// Move loader to drop off position
				else if (state == 6)
				{
					Print("State 6 - Move loader to drop off position");
	
					//Turn on move loader to drop off position
					SetLow(hashtableOutputs, "GPIO_27");
	
					// Wait for loader to move to drop off position
					WaitForInput(hashtableInputs, "GPIO_23", true);
	
					// Turn off move loader to drop off position
					SetHigh(hashtableOutputs, "GPIO_27");
	
					// Proceed to state 7
					state = 7;
				}
				// Release workpiece
				else if (state == 7)
				{
					Print("State 7 - Release workpiece");
	
					// Turn off vacuum gripper
					SetHigh(hashtableOutputs, "GPIO_05");
	
					// Turn on ejection air pulse
					SetLow(hashtableOutputs, "GPIO_06");
	
					// Sleep for 1 second
					Thread.sleep(1000);
	
					// Turn off ejection air pulse
					SetHigh(hashtableOutputs, "GPIO_06");
	
					// Proceed to state 8
					state = 8;
				}
				// Return to default position
				else if (state == 8)
				{
					Print("State 8 - Return to default position");
					
					// Turn on loader pickup
					SetLow(hashtableOutputs, "GPIO_26");
	
					// Sleep for 1 second
					Thread.sleep(1000);
	
					// Turn off loader pickup
					SetHigh(hashtableOutputs, "GPIO_26");
	
					// Proceed to state 1
					state = 1;
				}
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping station 1");
		}
	}
	
	// Runs 2nd station
	public static void RunStation2(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException{
		
		Print("FESTO Station 2");
		Print("Press 'CTRL + C' to quit");
		
		//Initialise state
		int state = 0;
		//Platform where the cap is picked up by station 3
		boolean platformEmpty = true;

		try{
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				// Wait
				if (state == 0)
				{
					Print("State 0 - Waiting");
				
					// If a workpiece is at the first light sensor
					if (CheckHigh(hashtableInputs, "GPIO_00") == true)
					{
						// If a workpiece is not at the second light sensor
						if (CheckHigh(hashtableInputs, "GPIO_03") == true)
						{
							// Proceed to state 1
							state = 1;
						}
						else
						{
							// If a workpiece is not at the last sensor
							if (CheckHigh(hashtableInputs, "GPIO_21") == true)	
							{
								// Proceed to state 2
								state = 2;
							}
							else
							{
								// Wait for 1 second
								Thread.sleep(1000);
							}
						}
					}
					else
					{
						// If a workpiece is not at the last light sensor
						if (CheckHigh(hashtableInputs, "GPIO_21") == true)
						{
							// If a workpiece is not at the second light sensor
							if (CheckHigh(hashtableInputs, "GPIO_03") == true)
							{
								// Wait for 1 second
								Thread.sleep(1000);
							}
							else
							{
								// Proceed to state 2
								state = 2;
							}
						}
						else
						{
							// Wait for 1 second
							Thread.sleep(1000);
						}
					}
				}
				// Move conveyor
				else if (state == 1)
				{
					Print("State 1 - Move conveyor");
					
					// Move conveyor belt
					SetLow(hashtableOutputs, "GPIO_05");
	
					// Wait for 1 second
					Thread.sleep(1000);
	
					// Stop moving conveyor belt
					SetHigh(hashtableOutputs, "GPIO_05");
	
					// If there is not a workpiece at the last light sensor
					if (CheckHigh(hashtableInputs, "GPIO_21") == true)
					{
						// Proceed to state 2
						state = 2;
					}	
					else
					{
						// Proceed to state 0
						state = 0;
					}
				}
				// Open gate, move conveyor, close gate, stop conveyor
				else if (state == 2)
				{
					Print("State 2 - Move conveyor and toggle gate");
					
					// Open gate
					SetLow(hashtableOutputs, "GPIO_04");
	
					// Move conveyor belt
					SetLow(hashtableOutputs, "GPIO_05");
	
					// Wait for 1 second
					Thread.sleep(1000);
	
					// Close gate
					SetHigh(hashtableOutputs, "GPIO_04");
	
					// Wait for 1 second
					Thread.sleep(1000);
				
					// Stop moving conveyor belt
					SetHigh(hashtableOutputs, "GPIO_05");
	
					// Proceed to state 0
					state = 0;
				}
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping Station 2");
		}
	}
	
	// Runs 3rd station
	public static void RunStation3(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException
	{
		Print("FESTO Station 3");
		Print("Press 'CTRL + C' to quit");
		
		// Initialise state
		int state = 0;
		// Bottle present at station 4
		boolean bottlePresent = true;

		try{
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				// Checking current state
				if (state == 0)
				{
					Print("State 0 - Assess current state");
					
					// Check if a workpiece is being gripped
					if (CheckHigh(hashtableInputs, "GPIO_03") == true)
					{
						// Check if it is at the dropoff position
						if(CheckHigh(hashtableInputs, "GPIO_21") == true)
						{
							// Check if the arm is raised
							// If arm has a lowered position sensor, this can be improved
							if(CheckHigh(hashtableInputs, "GPIO_25") == true)
							{
								// Proceed to state 7
								state = 7;
							}
							else
							{
								// Proceed to state 7
								state = 7;
							}
						}
						else
						{
							// Check if arm is raised
							if(CheckHigh(hashtableInputs, "GPIO_25") == true)
							{
								// Proceed to state 6
								state = 6;
							}
							else
							{
								// Proceed to state 5
								state = 5;
							}
						}
					}
					else
					{
						// Check if arm is arm is at middle position
						if(CheckHigh(hashtableInputs, "GPIO_22") == true)
						{
							// Proceed to state 1
							state = 1;
						}
						else
						{
							// Check if arm is raised
							if(CheckHigh(hashtableInputs, "GPIO_25") == true)
							{
								// Proceed to state 10
								state = 10;
							}
							else
							{
								// Proceed to state 9
								state = 9;
							}
						}
					}
				}
				// Wait for bottle to arrive at station 5
				else if (state == 1)
				{
					//Wait until there is a bottle present for capping at station 5
					while (bottlePresent == false)
					{
						//Wait
					}
					
					// Proceed to state 2
					state = 2;
				}
				// Wait for workpiece
				else if (state == 2)
				{
					Print("State 2 - Wait for workpiece");
					
					// If there is a piece waiting on the tray
					WaitForInput(hashtableInputs, "GPIO_00", true);

					state = 3;

				}
				// Move to pickup position
				else if (state == 3)
				{
					Print("State 3 - Move to pickup position");
					
					// Move to conveyor belt
					SetLow(hashtableOutputs, "GPIO_04");
					
					// Wait until it reaches the conveyor belt position
					WaitForInput(hashtableInputs, "GPIO_03", true);
					
					// Stop moving to conveyor belt
					SetHigh(hashtableOutputs, "GPIO_04");
					
					// Proceed to state 4
					state = 4;
				}
				// Pick up workpiece
				else if (state == 4)
				{
					Print("State 4 - Pick up workpiece");
					
					// Lower arm
					SetLow(hashtableOutputs, "GPIO_06");
		
					//Wait for 1 second
					Thread.sleep(1000);	
		
					// Grip workpiece
					SetHigh(hashtableOutputs, "GPIO_26");
		
					// Proceed to state 5
					state = 5;
				}
				// Raise arm at pickup position
				else if (state == 5)
				{
					Print("State 5 - Raise arm at pickup position");
					
					// Raise arm
					SetHigh(hashtableOutputs, "GPIO_06");
					
					//Wait until arm is raised
					WaitForInput(hashtableInputs, "GPIO_25", true);
					
					// Proceed to state 6
					state = 6;
				}
				// Move to drop off position
				else if (state == 6)
				{
					Print("State 6 - Move to dropoff position");
					
					// Move to drop off position
					SetLow(hashtableOutputs, "GPIO_05");
		
					// Wait until it reaches dropoff position
					WaitForInput(hashtableInputs, "GPIO_21", true);
		
					// Stop moving to drop off position
					SetHigh(hashtableOutputs, "GPIO_05");
		
					// Proceed to state 7
					state = 7;
				}
				// Lower arm at dropoff position
				else if (state == 7)
				{
					Print("State 7 - Lower arm at dropoff position");
					
					// Lower arm
					SetLow(hashtableOutputs, "GPIO_06");
		
					// Wait for 1 second
					Thread.sleep(1000);
					
					// Proceed to state 8
					state = 8;
				}
				// Drop off piece
				else if (state == 8)
				{
					Print("State 8 - Drop off workpiece");
					
					// Let go of workpiece
					SetLow(hashtableOutputs, "GPIO_26");

					// Proceed to state 9
					state = 9;
				}
				// Raise arm at dropoff position
				else if (state == 9)
				{
					Print("State 9 - Raise arm at dropoff position");
					
					// Raise arm
					SetHigh(hashtableOutputs, "GPIO_06");

					// Wait until arm is fully raised
					WaitForInput(hashtableInputs, "GPIO_25", true);
		
					// Send a signal to station 4 to let it know the arm has been raised
					
					// Proceed to state 10
					state = 10;
				}
				// Move to stationary position
				else if (state == 10)
				{
					Print("State 10 - Move to stationary position");
					
					// Move to conveyor belt
					SetLow(hashtableOutputs, "GPIO_04");
		
					// Wait until it reaches stationary position
					WaitForInput(hashtableInputs, "GPIO_22", true);
		
					// Stop moving to conveyor belt
					SetHigh(hashtableOutputs, "GPIO_04");
		
					// Proceed to state 1
					state = 1;
				}
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping Station 3");
		}
	}
	
	//Runs 4th station
	public static void RunStation4(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException{
			
		Print("FESTO Station 4");
		Print("Press 'CTRL + C' to quit");

		// Initialise state
		int state = 0;
		// If a cap has been placed on the bottle
		boolean capPlaced = true;

		try{
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				
				// Assess state
				if (state == 0)
				{
					//Wait for 1 second
					Thread.sleep(1000);
					
					//Proceed to state 1
				}
				/*
				// Move bottle onto rotary table
				else if (state == 1)
				{
					Print("State 1 - Move bottle onto rotary table");
					
					// If there is a bottle at this location
					if(CheckHigh(hashtableInputs, "") == true)
					{
						// Move conveyor
						SetLow(hashtableOutputs, "");
						
						// Wait for bottle to be in position
						WaitForInput(hashtableInputs, "", true);
						
						// Stop conveyor
						SetHigh(hashtableOutputs, "");
					}
					
					// Proceed to state 2
					state = 2;
				}
				// Rotate table
				else if (state == 2)
				{
					Print("State 2 - Rotate table");
					
					// Rotate table
					SetLow(hashtableOutputs, "");
					
					// Wait 1 second
					Thread.sleep(1000);
					
					// Stop rotating table
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 3
					state = 3;
				}
				// Place lid onto bottle
				else if (state == 3)
				{
					Print("State 3 - Place lid onto bottle");
					
					// If there is a bottle at this location
					if(CheckHigh(hashtableInputs, "") == true)
					{
						// Send signal to station 3 to place lid on bottle
					}
					
					// Proceed to state 4
					state = 4;
				}
				// Screw lid onto bottle
				else if (state == 4)
				{
					Print("State 4 - Screw lid onto bottle");
					
					// If there is a bottle at this location
					if(CheckHigh(hashtableInputs, "") == true)
					{
						// Lower arm
						SetLow(hashtableOutputs, "");
						
						// Wait 1 second
						Thread.sleep(1000);
						
						// Grip lid
						SetLow(hashtableOutputs, "");
						
						// Wait 1 second
						Thread.sleep(1000);
						
						// Rotate arm and tighten lid
						SetLow(hashtableOutputs, "");
						
						// Wait 1 second
						Thread.sleep(1000);
						
						// Stop rotating arm
						SetHigh(hashtableOutputs, "");
						
						// Stop gripping lid
						SetHigh(hashtableOutputs, "");
						
						// Raise arm
						SetHigh(hashtableOutputs, "");
						
						// Wait 1 second
						Thread.sleep(1000);
					}
					
					// Proceed to state 5
					state = 5;
				}
				// Move bottle off rotary table
				else if (state == 5)
				{
					Print("State 5 - Move bottle off rotary table");
					
					// If there is a bottle at this location
					if(CheckHigh(hashtableInputs, "") == true)
					{
						// Open gate
						SetLow(hashtableOutputs, "");
						
						// Wait for bottle to move away
						WaitForInput(hashtableInputs, "", true);
						
						// Close gate
						SetHigh(hashtableOutputs, "");
					}
					
					// Proceed to state 1
					state = 1;
				}
				*/ 
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping station");
		}
	}
	
	//Runs 5th station
	public static void RunStation5(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException{
			
		Print("FESTO Station 5");
		Print("Press 'CTRL + C' to quit");

		// Initialise state
		int state = 0;

		try{
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				// Wait for 1 second
				Thread.sleep(1000);
				
				/*
				// Assess state
				if (state == 0)
				{
					Print("State 0 - Assess state");
					
					// Wait for 1 second
					Thread.sleep(1000);
					
					// Check if a set of bottles is being gripped
					if(CheckHigh(hashtableInputs, "") == true)
					{
						// Proceed to state
					}
					else
					{
						// If there are 6 bottles waiting on the conveyor
						if(CheckHigh(hashtableInputs, "") == true)
						{
							// If there are only 3 bottles of the conveyor
							if(CheckHigh(hashtableInputs, "") == true)
							{
								// Proceed to state
							}
							else
							{
								// Proceed to state
							}
						}
						else
						{
							// If there are only 3 bottles of the conveyor
							if(CheckHigh(hashtableInputs, "") == true)
							{
								// Proceed to state
							}
							else
							{
								// Proceed to state
							}
						}
					}
				}
				else if (state == 1)
				{
					Print("State 1");
					
					// Turn on conveyor
					SetLow(hashtableOutputs, "");
					
					// Wait until second lot of bottles reach pickup zone
					WaitForInput(hashtableInputs, "", true);
					
					// Turn off conveyor
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 2
					state = 2;
				}
				else if (state == 2)
				{
					Print("State 2");
					
					// Proceed to state 3
					state = 3;
				}
				// Move 2nd row of bottles to picker zone
				else if (state == 3)
				{
					Print("State 3 - Move 2nd row of bottles to picker zone");
					
					// Turn on conveyor
					SetLow(hashtableOutputs, "");
					
					// Wait until second lot of bottles reach pickup zone
					WaitForInput(hashtableInputs, "", true);
					
					//Turn off conveyor
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 4
					state = 4;
				}
				else if (state == 4)
				{
					Print("State 4");
					
					// Move gripper to conveyor
					SetLow(hashtableOutputs, "");
					
					// Stop moving gripper to conveyor
					SetHigh(hashtableOutputs, "");
					
					// Lower gripper
					SetLow(hashtableOutputs, "");
					
					// Grip bottles
					SetLow(hashtableOutputs, "");
					
					// Raise gripper
					SetHigh(hashtableOutputs, "");
					
					// Move gripper to tray
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 4
					state = 4;
				}
				// Lower and release bottles
				else if (state == 5)
				{
					Print("State 5 - Lower and release bottles");
					
					// Lower gripper
					SetLow(hashtableOutputs, "");
					
					// Release gripper
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 6
					state = 6;
				}
				// Return to stationary position
				else if (state == 6)
				{
					Print("State 6 - Return to stationary position");
					
					// Raise gripper
					SetHigh(hashtableOutputs, "");
					
					// Move gripper to stationary position
					SetLow(hashtableOutputs, "");
					
					// Wait for 1 second
					Thread.sleep(1000);
					
					// Stop moving gripper to stationary position
					SetHigh(hashtableOutputs, "");
					
					// Proceed to state 1
					state = 1;
				}	
				*/
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping station 5");
		}
	}
	
	// Set listeners for all pins in use
	public static void AddListeners(Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		String inputPin;
		
		Set<String> inputKeys = hashtableInputs.keySet();
		
		//Obtaining iterator over set entries
    	Iterator<String> inputItr = inputKeys.iterator();
 
    	//Displaying Key and value pairs
    	while (inputItr.hasNext())
    	{ 
    		// Getting Key
    		inputPin = inputItr.next();

            GpioPinDigitalInput input = hashtableInputs.get(inputPin);
            
            // Add Listener for logging
    		System.out.println("Listener added to input for logging: " + inputPin);
    		input.addListener(new GpioLoggingListener());
            
    		/*
            // Add listener for BeSPaceD
            System.out.println("Listener added to input for BeSPaceD: " + inputPin);
            input.addListener(new GpioBespacedListener());
            */
        }
		
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
    	//Obtaining iterator over set entries
    	Iterator<String> outputItr = outputKeys.iterator();
 
    	//Displaying Key and value pairs
    	while (outputItr.hasNext())
    	{ 
    		// Getting Key
    		outputPin = outputItr.next();
            
            GpioPinDigitalOutput output = hashtableOutputs.get(outputPin);

            // Add Listener for logging
            System.out.println("Listener added for output: " + outputPin);
    		hashtableOutputs.get(outputPin).addListener(new GpioLoggingListener());

    		/*
            // Add listener for BeSPaceD
            System.out.println("Listener added to output for BeSPaceD: " + outputPin);
            output.addListener(new GpioBespacedListener());
            */
        }
	}
	
	// Check if an input is high
	public static boolean CheckHigh(Hashtable<String, GpioPinDigitalInput> hashtable, String pin)
	{
		//Check if the pin is high
		System.out.println(hashtable.get(pin).isHigh());
		return hashtable.get(pin).isHigh();
	}
	
	// Check if an input is low
	public static boolean CheckLow(Hashtable<String, GpioPinDigitalInput> hashtable, String pin)
	{
		//Check if the pin is high
		return hashtable.get(pin).isLow();
	}
    
    
    
    // ======================================== LOGGING ====================================== //
	
	// Logging : State change listener
	public static class GpioLoggingListener implements GpioPinListenerDigital
	{
		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event)
		{
			// Display pin state on console
			System.out.println("GPIO Pin State Change: " + event.getPin() + " = " + event.getState());
		}
	}
	
    
   /* 
    
    // ======================================= BeSPaceD ===================================== //
 
    final static FestoData festoData = new FestoData();

    // BeSpaceD : State change listener
    public static class GpioBespacedListener implements GpioPinListenerDigital
    {
        @Override
        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event)
        {
            GpioPin gpioPin = event.getPin();
            PinState state = event.getState(); // enum HIGH or LOW
            
            // Display pin state on console
            System.out.println("Send Pin State Change to BeSPaceD: " + gpioPin + " = " + state);
            
            
            
            // ------------------------------------------------------ Import this data into BeSpaceD
            
            // Convert raw GPIO data into usable strings
            String pinName = gpioPin.getPin().getName();
            String stateName = state.getName();
            
            // Convert GPIO data into Festo data
            String festoDevice = festoData.pinToDevice(pinName);
            Float festoValue  = festoData.stateToValue(festoDevice, stateName);
            
            // TODO: Call BeSpaceD "Festo Import"
            FestoServer.importSignal(festoDevice, festoValue);

            System.out.println("Imported into BeSPaceD: " + festoDevice + ": " + festoValue);
        }
    }
    */
    
    // Print message to console
	public static void Print(String message)
	{
		System.out.println(message);
	}
	
	// Print all input states to console
	public static void PrintAllInputStates(Hashtable<String, GpioPinDigitalInput> hashtableInputs)
	{			
		String inputPin;
		
		Set<String> inputKeys = hashtableInputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> inputItr = inputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (inputItr.hasNext())
	    { 
	    	// Getting Key
	    	inputPin = inputItr.next();
	    	
	    	//Print the status
	    	System.out.println("Pin: " + inputPin + " = " + hashtableInputs.get(inputPin).getState());
	    } 
	}
	
	// Print all output states to console
	public static void PrintAllOutputStates(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{			
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();
	    	
	    	//Print the status
	    	System.out.println("Pin: " + outputPin + " = " + hashtableOutputs.get(outputPin).getState());
	    } 
	}
	
	// Set all output pins to high
	public static void SetAllHigh(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{	
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();
	    	
	    	//Set the pin to high
			hashtableOutputs.get(outputPin).high();
	    } 
	}
	
	// Set all output pins to low
	public static void SetAllLow(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{	
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();
	    	
	    	//Set the pin to low
			hashtableOutputs.get(outputPin).low();
	    } 
	}
	
	// Set an output pin to high
	public static void SetHigh(Hashtable<String, GpioPinDigitalOutput> hashtable, String pin)
	{
		//Check if the pin is high
		hashtable.get(pin).high();
	}
	
	// Set an output pin to low
	public static void SetLow(Hashtable<String, GpioPinDigitalOutput> hashtable, String pin)
	{
		//Check if the pin is high
		hashtable.get(pin).low();
	}	
	
	// Waits for an inputs condition to be met before proceeding
	public static void WaitForInput(Hashtable<String, GpioPinDigitalInput> hashtableInputs, String pin, boolean state)
	{
		while (CheckLow(hashtableInputs, pin) == state)
		{
			// Waiting...
		}
	}	
	
	// Set shutdown options for output pins
	public static void SetShutdownOptions(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries 
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();

	    	System.out.println("Shutdown setting added for output: " + outputPin);
	    	hashtableOutputs.get(outputPin).setShutdownOptions(true, PinState.HIGH, PinPullResistance.OFF);
	    } 
	}
}


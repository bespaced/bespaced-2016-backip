/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._
import PhysicalModel._



class FestoActuator(name: String) extends Actuator[ID](name)
object FestoActuator { def apply(name:String): FestoActuator = { new FestoActuator(name) } }

object Actuators {

    // Station 1
    val StackEjectorExtend = FestoActuator("Stack Ejector Extend")
    val VacuumGrip         = FestoActuator("Vacuum Grip"         )
    val EjectAirPulse      = FestoActuator("Eject Air Pulse"     )
    val LoaderPickup       = FestoActuator("Loader Pickup"       )
    val LoaderDropoff      = FestoActuator("Loader Dropoff"      )
    
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

  type FestoActuatorComponent  = Component[FestoActuator]
  type FestoActuatorOccupyNode = OccupyNode[FestoActuator]
  
}
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * The "model" module is for defining the domain specific modeling types and values for the Festo Mini Factory
 */

package BeSpaceDFesto

import java.util.Date

import BeSpaceDCore._
import model.PhysicalModel._



package object model {  
  
  val core = standardDefinitions; import core._
  
  // ==================================================================================== Festo Object Identity
  
  // Identity
  
  type ID = String
  

  // ==================================================================================== Festo Devices
  
  // Devices are parts, actuators and sensors.
  // Devices are defined by an enumeration using case classes and values.
    
  class FestoDevice(id: ID) extends Component[ID](id)
  type FestoDeviceComponent  = Component[FestoDevice]
  type FestoDeviceOccupyNode = OccupyNode[FestoDevice]

  import Parts._
  import Actuators._
  import Sensors._
  
  
  
  // ==================================================================================== Festo Materials
  
  trait FestoMeterial extends Material[ID]
  
  type FestoMeterialComponent  = Component[FestoMeterial]
  
  
  
  // ------------------------------------------------------------------------------------ Discrete

    class FestoDiscreteMaterial(id: String) extends DiscreteMaterial[ID](id)
    object FestoDiscreteMaterial { def apply(id:String) = new FestoDiscreteMaterial(id) }
    
    val Bottle  = FestoDiscreteMaterial("Bottle")
    val Cap     = FestoDiscreteMaterial("Cap")
    
    
    type FestoDiscreteMaterialComponent  = Component[FestoDiscreteMaterial]
    type FestoDiscreteMaterialOccupyNode = OccupyNode[FestoDiscreteMaterial]
  
  
 
  // ------------------------------------------------------------------------------------ Analog  

    abstract class FestoAnalogMaterial(name:String, override val unit: String, override val quantity: Double) extends AnalogMaterial[ID](name)
    
    class Corn(override val quantity: Double) extends FestoAnalogMaterial("Corn Kernels","g",  quantity)
    object Corn { def apply(quantity: Double) = new Corn(quantity) }

    val NoCorn = Corn(0)
    
    class Water(override val quantity: Double) extends FestoAnalogMaterial("Water", "ml", quantity)
    object Water { def apply(quantity: Double) = new Water(quantity) }

    val NoWater = Water(0)
    
    
    type FestoAnalogMaterialComponent  = Component[FestoAnalogMaterial]
    type FestoAnalogMaterialOccupyNode = OccupyNode[FestoAnalogMaterial]
  
  
  
   // ==================================================================================== Festo States

    abstract class FestoSignal
    case object High extends FestoSignal  { override def toString = "High" }
    case object Low  extends FestoSignal  { override def toString = "Low" }
    
        
    abstract class FestoDeviceState(signal: FestoSignal) extends DeviceState[FestoSignal](signal)
    
    abstract class FestoActuatorState(signal: FestoSignal) extends ActuatorState(signal)
    object Active  extends FestoActuatorState(Low)
    object Passive extends FestoActuatorState(High)
    
    abstract class FestoSensorState(signal: FestoSignal) extends SensorState(signal)

    abstract class FestoLightSensorState(signal: FestoSignal) extends FestoSensorState(signal)
    object Obstructed extends FestoLightSensorState(High)
    object Unobstructed  extends FestoLightSensorState(Low)
    
    abstract class FestoGripSensorState(signal: FestoSignal) extends FestoSensorState(signal)
    object Gripped extends FestoGripSensorState(High)
    object Loose  extends FestoGripSensorState(Low)
    
    
  
  // ==================================================================================== Festo Spatial Relationships
    
    // TODO:
    //       Think about conveyer belts. e.g. break up the belt into three parts: beginning, middle and end.
    //       When the loader is in the drop-off state it has Proximity to the beginning of the conveyer belt.
    //       So topology may involve the STATE of the device not just the device.

    
    // ---------------------------------------------------------------------------------- Festo Domain Specific Relationships
    
    abstract class FestoConnection extends Jsonable
    
    // Actuator Connections
    case object Attached extends FestoConnection                        { override def toJson = "\"Attached\"" }
    case class  Proximity(distance: Double = 0) extends FestoConnection { override def toJson = "{\"type\"=\"Proximity\",\"distance\"=\"$distance\"}" }

    val attached = SpatialRelationshipType(Attached)
    val proximity = SpatialRelationshipType(Proximity)

    // Sensor Connections
    case object ProcessSequence extends FestoConnection                 { override def toJson = "\"ProcessSequence\"" }
    case object MaterialFlow extends FestoConnection                    { override def toJson = "\"MaterialFlow\"" }
    
    val processSequence = SpatialRelationshipType(ProcessSequence)
    val materialFlow = SpatialRelationshipType(MaterialFlow)
  
    
  // ==================================================================================== Festo Process Topology
    
    // Topology for Sensors ONLY
    
    // KF & GP Notes: We decided that for the Visualisation of sensors in the Festo Mini Factory
    //                that the semantic relationship between them that makes the most sense is PROCESS.
    //                i.e. the chronological sequence that would normally take place in the manufacturing process.
    
    // KF Notes: 1. Actually the sequence cycles around as the ejector will be retracted before the
    //              work piece is gripped and after drop off it will go back to the start of the sequence.


    // ------------------------------------------------------------------------------------ Spatial

//    def edge2[N](s: N, t: N) = EdgeAnnotated(s, t, Some(processSequence))
//
//    implicit class EdgeOps[N <: Invariant, L <: N](private val left: L) extends AnyVal {
//      @inline def -->[R <: N] (right : R) = edge2(left, right)
//    }
    
    val sensorsProcessTopology_Station1: SpatialTopology[ID, FestoConnection] =
    {
      @inline def edge(s: FestoSensor, t: FestoSensor) = EdgeAnnotated(s, t, Some(ProcessSequence))
      
      BeGraphAnnotated[FestoSensor, FestoConnection] (
          edge(StackEjectorRetracted , StackEjectorExtended)  ^
          edge(StackEjectorExtended  , StackEmpty)            ^
          edge(StackEjectorExtended  , StackEjectorRetracted) ^
          edge(StackEjectorExtended  , WorkpieceGripped)      ^
          edge(WorkpieceGripped      , LoaderPickedUp)        ^
          edge(LoaderPickedUp        , LoaderDroppedOff)      ^
          edge(LoaderDroppedOff      , StackEjectorExtended)
          )
    }
    

    
    // KF Notes: 2. What happens when more caps are added to the tube? StackEmpty signal goes Low.
    //              This implies we need to include the sensor state as part of the node value.
    //              This would be a stateful process topology (see below)

    // ------------------------------------------------------------------------------------ Stateful
    import SensorsInState._
    
    type FestoSensorsStatefulTopology = SensorsStatefulTopology[ID, FestoSignal, FestoConnection]
    
    val sensorsStatefulProcessTopology: FestoSensorsStatefulTopology =
    {
      @inline def edge(s: FestoSensorInState, t: FestoSensorInState) = EdgeAnnotated(s, t, Some(processSequence))
      
      BeGraphAnnotated[FestoSensorInState, SpatialRelationshipType[FestoConnection]] (
          edge(StackEjectorExtendedOn  , StackEmptyOn)            ^
          edge(StackEjectorExtendedOn  , StackEjectorExtendedOff) ^
          edge(StackEjectorExtendedOn  , StackEjectorRetractedOn) ^
          edge(StackEjectorExtendedOn  , WorkpieceGrippedOn)      ^
          edge(WorkpieceGrippedOn      , LoaderPickedUpOn)        ^
          edge(LoaderPickedUpOn        , LoaderDroppedOffOn)      ^
          edge(LoaderDroppedOffOn      , WorkpieceGrippedOff)     ^
          edge(WorkpieceGrippedOff     , StackEjectorExtendedOn)
          )
    }

  
  
  // ==================================================================================== Festo Temporal Relationships
  
  // TODO...
    
    
  // ==================================================================================== Generate Data Sets
    
    
    def main(args: Array[String])
    {
      val core = standardDefinitions; import core._
      
      // Archive the spatial topology as JSON
      val sensorsProcessTopologyJson_Station1: String  = toJson(sensorsProcessTopology_Station1)
      
      println("Process Topology for Sensors Only of Festo Station 1")
      println("---------------------------------------------------")
      println(sensorsProcessTopologyJson_Station1)
      println("---------------------------------------------------")
      
      
      // Archive some example sensor events as JSON
      class FestoSensorEvent(sensor: FestoSensor, timepoint: Date, state: FestoSensorState) extends SensorEvent[ID, Date, FestoSensorState](sensor, timepoint, state) 
      object FestoSensorEvent { def apply(s: FestoSensor, t: Date, v: FestoSensorState) = new FestoSensorEvent(s,t,v) }
      
      val se1 = FestoSensorEvent(StackEjectorRetracted, new Date(), Obstructed  )
      val se2 = FestoSensorEvent(WorkpieceGripped,      new Date(), Unobstructed)
      val se3 = FestoSensorEvent(LoaderPickedUp,        new Date(), Obstructed)
      val se4 = FestoSensorEvent(StackEmpty,            new Date(), Unobstructed)
      
      val exampleSensorEvents_Station1 = se1 ^ se2 ^ se3 ^ se4
        
      val exampleSensorEventsJson_Station1: String  = toJson(exampleSensorEvents_Station1)
      
      println("Example Sensor Events of Festo Station 1")
      println("---------------------------------------------------")
      println(exampleSensorEventsJson_Station1)
      println("---------------------------------------------------")
      
    }
    
}









package BeSpaceDExternalExample_DO_NOT_SHARE


import BeSpaceDCore._
object GraphTest1 extends GraphOperations{
	
  
  
  def exampleGraph1 = BIGAND(Nil)

			def exampleGraph2 = BIGAND(
					Edge(1,2)::
						Edge(2,3)::
							Nil)

							def exampleGraph3 = BIGAND(
									Edge(1,2)::
										Edge(2,3)::
											Edge(3,4)::
												Edge(4,5)::
													Edge(5,6)::
														Edge(4,7)::
															Nil)

															def exampleGraph4 = BIGAND(
																	Edge(1,2)::
																		Edge(2,3)::
																			Edge(3,4)::
																				Edge(4,5)::
																					Edge(5,6)::
																						Edge(4,7)::
																							Edge(6,1)::
																								Nil)
																								def exampleGraph5 = BIGAND(
																										Edge(1,2)::
																											Edge(2,3)::
																												Edge(3,4)::  
																													Edge(4,5)::
																														Edge(5,6)::
																															Edge(6,7)::
																																Edge(7,8)::
																																	Edge(8,9)::
																																		Edge(6,10)::
																																			Edge(10,11)::
																																				Edge(11,12)::
                                                                        Edge(10,13)::
																																					Nil)


																							def main(args: Array[String]) {
		println(this.transitiveHull(invariant2Tuplelist(exampleGraph1)).contains((1,1)))
		println("-----")
		println(invariant2Tuplelist(exampleGraph2))
		println(this.transitiveHull(invariant2Tuplelist(exampleGraph2)))
		println(this.transitiveHull(invariant2Tuplelist(exampleGraph2)).contains((1,3)))
		println("-----")
    println(invariant2Tuplelist(exampleGraph3))
    println(this.transitiveHull(invariant2Tuplelist(exampleGraph3)))
    println("-----")
    println(invariant2Tuplelist(exampleGraph4))
    println(this.transitiveHull(invariant2Tuplelist(exampleGraph4)))
    println("-----")
    println(invariant2Tuplelist(exampleGraph5))
    println(this.transitiveHull(invariant2Tuplelist(exampleGraph5)))
    println(this.transitiveHull(invariant2Tuplelist(exampleGraph5)).contains((6,13)))
	}
}
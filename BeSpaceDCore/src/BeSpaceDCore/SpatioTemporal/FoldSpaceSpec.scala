package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore._
import BeSpaceDCore.Log._


class FoldSpaceSpec extends UnitSpec {

  val core = standardDefinitions; import core._

  val cloud = Owner("Cloud")
  val mountain = Owner("Mountain")

  /**
   * This function is used as a parameter to the fold of space.
   * It finds the amount of cloudy area in the invariant and add that to the cumulative total.
   *
   * @param total The cumulative total area - must be passed in ach call within the fold
   * @param invariant The data source invariant. Must be of the form IMPLIES(Owner, point(s)...)
   *                  points may be a single OccupyPoint, an AND of two OccupyPoints or a BIGAND of OccupyPoints.
   * @return The total area. Technically this is the total number of OccupyPoints found with an owner of "Cloud".
   */
  def addCloudyArea(total: Int, invariant: Invariant): Int =
  {
    def isCloudyArea(owner: Owner[Any]): Boolean = owner == cloud

    def calculateArea(list: List[Invariant]): Int = {
      val areas: List[Int] = list map {
        inv: Invariant =>
          inv match {
            case IMPLIES(owner: Owner[Any], point: OccupyPoint) => if (isCloudyArea(owner)) 1 else 0
            case IMPLIES(owner: Owner[Any], AND(p1: OccupyPoint, p2: OccupyPoint)) => if (isCloudyArea(owner)) 2 else 0
            case IMPLIES(owner: Owner[Any], BIGAND(points: List[OccupyPoint])) => if (isCloudyArea(owner)) points.length else 0
            case _ => 0
          }
      }

      areas.sum
    }

    val area = invariant match {
      case AND(t1, t2) => calculateArea(t1 :: t2 :: Nil)
      case BIGAND(sublist: List[Invariant]) => calculateArea(sublist)
      case _ => 0
    }

    val result = total + area

    testOn {
      println(s"space func()...")
      println(s"    previous total = $total")
      println(s"              item = $invariant")
      println(s"              area = $area")
      println(s"   resulting total = $result")
    }

    result
  }

  "Spatio-Temporal Operator: foldSpace" should "be able to fold space with a simple example" in {

    // Boxes
    val b1 = OccupyBox(1, 1, 10, 10)
    val b2 = OccupyBox(5, 5, 15, 15)

    // Space owner-occupied
    val s1 = IMPLIES(mountain, b1)
    val s2 = IMPLIES(cloud, b2)

    val spaceSeries = List(s1, s2) //, s3, s4)
    val conjunction = BIGAND(spaceSeries)
    val initialValue = 0

    val startBox = OccupyBox(1, 1, 5, 5)
    val stopBox = OccupyBox(1, 1, 5, 5)
    val step: Translation = (5, 5)

    val foldedSpace = foldSpace[Int](
      conjunction,
      initialValue,
      startBox, stopBox, step,
      addCloudyArea
    )

    // Answer: The only overlap between OccupyBox(1,1,5,5) and the cloud OccupyBox(5,5,10,10) is OccupyPoint(5,5).
    //         So the total area should be 1.
    //
    assertResult(expected = 1)(actual = foldedSpace)
  }

  it should "be able to fold space with a complex example" in {

    // Boxes
    val b1 = OccupyBox(1, 1, 10, 10)
    val b2 = OccupyBox(5, 5, 15, 15)
    val b3 = OccupyBox(10,10, 20,20)
    val b4 = OccupyBox(21,21, 30,30)

    // Space owner-occupied
    val s1 = IMPLIES(mountain, b1)
    val s2 = IMPLIES(cloud, b2)
    val s3 = IMPLIES(cloud, b3)
    val s4 = IMPLIES(mountain, b4)

    val spaceSeries = List(s1, s2 , s3, s4)
    val conjunction = BIGAND(spaceSeries)
    val initialValue = 0
    //val normalisedData = normalize(unfoldInvariant(conjunction))
    val normalisedData = normalizeOwnerOccupied(unfoldInvariant2(conjunction))

    debugOn
    {
      println(s"normalisedData = $normalisedData")
    }

    val startBox = OccupyBox(1, 1, 5, 5)
    val stopBox  = OccupyBox(26,26,30,30)
    val step: Translation = (5, 5)

    val foldedSpace = foldSpace[Int](
      normalisedData,
      initialValue,
      startBox, stopBox, step,
      addCloudyArea
    )

    // Answer:
    //    The overlap between OccupyBox(1,1,5,5) and clouds OccupyBox(5,5,15,15) U OccupyBox(10,10,20,20) is OccupyPoint(5,5).
    //    The overlap between OccupyBox(6,6,10,10) and clouds OccupyBox(5,5,15,15) U OccupyBox(10,10,20,20) is OccupyPoint(6..10,6..10).
    //    The overlap between OccupyBox(11,11,15,15) and clouds OccupyBox(5,5,15,15) U OccupyBox(10,10,20,20) is OccupyPoint(11..15,11..15).
    //    The overlap between OccupyBox(16,16,20,20) and clouds OccupyBox(5,5,15,15) U OccupyBox(10,10,20,20) is OccupyPoint(16..20,16..20).
    //    The overlap between OccupyBox(21,21,25,25) and clouds OccupyBox(5,5,15,15) U OccupyBox(10,10,20,20) is nothing.
    //    Same for the rest.
    // So the total area should be 1 + 25 + 25 + 25 = 76.
    //
    assertResult(expected = 76)(actual = foldedSpace)
  }

}



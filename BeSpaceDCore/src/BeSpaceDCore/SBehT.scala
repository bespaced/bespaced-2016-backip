
/*
 * Jan Olaf Blech
 * 
 * 2014
 * 
 * Spatial Behavioral Types 
 * 
 * 
 * History:
 * (moved from core definitions)
 */
package BeSpaceDCore



/* Specification Information
 * 
 * Begin BehT
 * 
 */


/*
 * Spatio Behavioral Types: Meta Information
 */

case class SpatioBehavioralTypeSystem (aspects : List[String])

/* 
 * Spatio Behavioral Types : The Type Constructors
 */

abstract class SpatioTemporalBehavioralType
abstract class GeometricBehavioralType extends SpatioTemporalBehavioralType

/*
 * Typical type operations, some may be casted to or lifted from the invariant level
 */

case class TopBehavioralType() 
	extends SpatioTemporalBehavioralType // Top element in a type semi-lattice
case class RecordBehavioralType (id:String, sbehts : List[SpatioTemporalBehavioralType] ) 
	extends SpatioTemporalBehavioralType // corresponds to component composition
case class NamedRecordBehavioralType (id:String, names : List[String], sbehts : Map[String,SpatioTemporalBehavioralType] ) 
	extends SpatioTemporalBehavioralType // corresponds to named record like component composition
case class AndBehavioralType (id:String, sbeht1 : SpatioTemporalBehavioralType, sbeht2 : SpatioTemporalBehavioralType) 
	extends SpatioTemporalBehavioralType // corresponds to component composition (2 elements)
case class UnionBehavioralType (id:String, sbehts : List[SpatioTemporalBehavioralType]) 
	extends SpatioTemporalBehavioralType // corresponds to alternative components
case class OrBehavioralType (id:String, sbeht1 : SpatioTemporalBehavioralType, sbeht2 : SpatioTemporalBehavioralType) 
	extends SpatioTemporalBehavioralType // corresponds to alternative composition (2 elements)
// add dependent types

/* Only used type as of 10/02/2014:
 * A component with attachment points
 */
case class SimpleSpatioTemporalBehavioralType (id : String, behavior : Invariant, attachmentpoints : List[String],attachmentpointsbehavior : Map[String,Invariant]) extends GeometricBehavioralType

/*
 * more refined version, not in use as of 12/02/2014
 */ 



case class GeometricOffset3D (offset : Occupy3DPoint, beht: GeometricBehavioralType) extends GeometricBehavioralType

case class GeometricSystemBehavioralType (id : String, behavior : Invariant, attachmentpoints : List[String],attachmentpointsbehavior : Map[String,GeometricBehavioralType]) extends GeometricBehavioralType

// with explicit cycle time

case class GeometricOffset3DC[T] (offset : Occupy3DPoint, beht: GeometricBehavioralType, cycletime : T) extends GeometricBehavioralType

case class GeometricOffsetTimeC[T] (offset : T, beht: GeometricBehavioralType, cycletime : T) extends GeometricBehavioralType // as suggested by Heinz


case class GeometricSystemBehavioralTypeC[T] (id : String, behavior : Invariant, attachmentpoints : List[String],attachmentpointsbehavior : Map[String,GeometricBehavioralType], cycletime : T) extends GeometricBehavioralType
case class GeometricSystemBehavioralTypeAttachC[T] (
    id : String, 
    behavior : Invariant, 
    attachmentpoints : List[String],
    attachmentpointsbehavior : Map[String,GeometricBehavioralType], 
    attachedcomponentsbehavior : Map[String,GeometricBehavioralType], 
    cycletime : T) extends GeometricBehavioralType
    
    

    
/*
 * End BehT
 */

class SBehT {
/*
 * BehT helpers:
 */ 
    
    //Least common multiple (int based cycle time)


    def lcm(i : Int,j : Int) : Int ={
		  var k = i;
		  while ( ((k % i) != 0) && ((k % j) != 0)) {
			  k+=1;
		    }
		  k
	}
}